<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <link href="resources/css/main.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Velosipēdu noma</title>
</head>
<body>
<div align="center">
    <table border="0">
        <tr>
            <td colspan="2" align="center"><h2>Noma ir piereģistrēta!</h2></td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <h3>Informācija par nomas veikšanu:</h3>
            </td>
        </tr>
        <tr>
            <td>Vārds:</td>
            <td>${rent.name}</td>
        </tr>
        <tr>
            <td>Uzvārds:</td>
            <td>${rent.surname}</td>
        </tr>
        <tr>
            <td>Velosipēda ID numurs:</td>
            <td>${rent.bikeId}</td>
        </tr>
        <tr>
            <td>Nomas uzsākšanas laiks:</td>
            <td>${rent.timeStarted.toLocaleString()}</td>
        </tr>
        <tr>
            <td>Nomas pabeigšanas laiks:</td>
            <td>${rent.timeFinished.toLocaleString()}</td>
        </tr>

    </table>
    <br>
    <br>
    <a href="${pageContext.request.contextPath}/rent">Atpakaļ</a>
</div>
</body>
</html>