<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <link href="resources/css/main.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Velosipēdu noma</title>
</head>
<body>
<div align="center">
    <form action="register" method="post" modelAttribute="rentForm">
        <table class="form-table">
            <tr>
                <td colspan="2"><h2>Reģistrēt jauno nomu:</h2></td>
            </tr>
            <tr>
                <td>Vārds:</td>
                <td><input path="name" name="name" id="name" pattern="[A-Za-z]{1,32}"/></td>
            </tr>
            <tr>
                <td>Uzvārds:</td>
                <td><input path="surname" name="surname" id="surname" pattern="[A-Za-z]{1,32}"/></td>
            </tr>
            <tr>
                <td>Velosipēda ID numurs:</td>
                <td>
                    <select path="bikeId" name="bikeId" id="bikeId">
                        <option disabled selected value> -- Izvēlēties velosipēdu --</option>
                        <option value="1">Nr. 1</option>
                        <option value="2">Nr. 2</option>
                        <option value="3">Nr. 3</option>
                        <option value="4">Nr. 4</option>
                        <option value="5">Nr. 5</option>
                    </select>
            </tr>
            <tr>
                <td>Nomas uzsākšanas laiks:</td>
                <td><input type="datetime-local" path="timeStarted" name="timeStarted" id="timeStarted"
                           pattern="yyyy-MM-dd'T'HH:mm"/></td>
            </tr>
            <tr>
                <td>Nomas pabeigšanas laiks:</td>
                <td><input type="datetime-local" path="timeFinished" name="timeFinished" id="timeFinished"
                           pattern="yyyy-MM-dd'T'HH:mm"/></td>
            </tr>
            <tr>
                <td></td>
            </tr>
            <tr>
                <td colspan="2"><input type="submit" value="OK" class="button"/></td>
            </tr>
        </table>
    </form>
    <br>
    <div class="errors">
        <c:if test="${errors != null}">
            <c:forEach items="${errors}" var="err">
                <p>${err}</p>
            </c:forEach>
        </c:if>
    </div>
    <br>
    <div>
        <table class="list-table">
            <thead>
            <tr>
                <td colspan="6">Reģistrēto nomu saraksts:</td>
            </tr>
            </thead>
            <tbody>
            <tr class="col-headers">
                <td>Velosipēda ID</td>
                <td>Vārds</td>
                <td>Uzvārds</td>
                <td>Nomas uzsākšanas laiks</td>
                <td>Nomas pabeigšanas laiks</td>
                <td></td>
            </tr>
            <c:forEach items="${rents}" var="rents">
                <tr>
                    <td><c:out value="${rents.bikeId}"/></td>
                    <td><c:out value="${rents.name}"/></td>
                    <td><c:out value="${rents.surname}"/></td>
                    <td><c:out value="${rents.timeStarted.toLocaleString()}"/></td>
                    <td><c:out value="${rents.timeFinished.toLocaleString()}"/></td>
                    <td><a href="${pageContext.request.contextPath}/rent/delete/${rents.rentId}"
                           class="button">Dzēst</a></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>

</div>
</body>
</html>