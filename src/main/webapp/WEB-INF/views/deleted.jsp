<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <link href="/resources/css/main.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Velosipēdu noma</title>
</head>
<body>
<div align="center">
    <h2>Noma tika izdzēsta!</h2>
    <br>
    <br>
    <a href="${pageContext.request.contextPath}/rent">Atpakaļ</a>
</div>
</body>
</html>