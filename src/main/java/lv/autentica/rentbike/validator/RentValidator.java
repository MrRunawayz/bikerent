package lv.autentica.rentbike.validator;

import lv.autentica.rentbike.dao.RentDao;
import lv.autentica.rentbike.model.Rent;
import lv.autentica.rentbike.service.RentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.Calendar;
import java.util.Date;


@Component
public class RentValidator implements Validator {

    @Autowired
    private RentService rentService;

    @Override
    public boolean supports(Class<?> aClass) {
        return RentDao.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "Lūdzu aizpildiet lauku 'Vārds'!");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "surname", "Lūdzu aizpildiet lauku 'Uzvārds'!");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "bikeId", "Lūdzu aizpildiet lauku 'Velosipēda ID numurs'!");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "timeStarted", "Lūdzu aizpildiet lauku 'Nomas uzsākšanas laiks'!");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "timeFinished", "Lūdzu aizpildiet lauku 'Nomas pabeigšanas laiks'!");
        if (!errors.hasErrors()) {
            Rent rent = (Rent) o;
            if (rent.getTimeStarted() != null && rent.getTimeFinished() != null) {
                Date date = new Date();
                if (rent.getTimeFinished().before(rent.getTimeStarted()) || rent.getTimeStarted().before(date)) {
                    errors.rejectValue("timeStarted", "Lūdzu pārbaudiet nomas uzsākšanas un pabeigšanas laiku pareizību!");
                } else
                    for (Rent record : rentService.getCurrentBikeRents(rent.getBikeId(), rent.getTimeStarted())) {
                        if (rent.getTimeStarted().before(record.getTimeFinished()) && record.getTimeStarted().before(rent.getTimeFinished())) {
                            errors.rejectValue("timeStarted", "Dotajā laika posmā no " + rent.getTimeStarted().toLocaleString() + " līdz " + rent.getTimeFinished().toLocaleString() +
                                    " izvēlētais velosipēds ir aizņemts! Lūdzu izvēlēties citu laiku vai citu velosipēdu.");
                            break;
                        }
                    }
                Calendar cal = Calendar.getInstance();
                cal.set(2038, Calendar.JANUARY, 1);
                Date limitDate = cal.getTime();
                if (rent.getTimeStarted().after(limitDate) || rent.getTimeFinished().after(limitDate)) {
                    errors.rejectValue("timeStarted", "Lūdzu pārbaudiet nomas uzsākšanas un pabeigšanas laiku pareizību!");
                }
            }
        }
    }
}
