package lv.autentica.rentbike.controller;

import lv.autentica.rentbike.model.Rent;
import lv.autentica.rentbike.service.RentService;
import lv.autentica.rentbike.validator.RentValidator;
import lv.autentica.rentbike.exception.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/")
public class RentBikeController {

    @Autowired
    private RentService rentService;

    @Autowired
    private RentValidator validator;

    @RequestMapping(value = {"/", "/rent"}, method = RequestMethod.GET)
    public String viewRent(ModelMap model) {
        List<Rent> rents = rentService.findAllRents();
        model.addAttribute("rents", rents);
        Rent rentForm = new Rent();
        model.addAttribute("rentForm", rentForm);
        return "form";
    }

    @RequestMapping(value = "/rent/delete/{rentId}")
    public ModelAndView deleteRent(@PathVariable Integer rentId) {
        ModelAndView modelAndView = new ModelAndView("deleted");
        rentService.deleteRent(rentId);
        return modelAndView;
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
    }

    @RequestMapping(value = {"/", "/register"}, method = {RequestMethod.POST, RequestMethod.GET})
    public String rentProcess(Rent rent, BindingResult result,
                              ModelMap model) {
        List<String> errors = new ArrayList<>();
        validator.validate(rent, result);
        if (result.hasErrors()) {
            errors.addAll(result.getAllErrors().stream()
                    .map(ObjectError::getCode)
                    .collect(Collectors.toList()));
            throw new ValidationException(errors);
        }
        try {
            rentService.saveRent(rent);
            model.addAttribute(rent);
            return "rentSuccess";
        } catch (Exception e) {
            errors.add(e.getMessage());
            model.addAttribute("errors", errors);
            return "form";
        }
    }

    @ExceptionHandler(ValidationException.class)
    public ModelAndView handleValidationException(ValidationException ex) {
        ModelAndView modelAndView = new ModelAndView();
        List<Rent> rents = rentService.findAllRents();
        Rent rentForm = new Rent();
        modelAndView.addObject("rents", rents);
        modelAndView.addObject("rentForm", rentForm);
        modelAndView.addObject("errors", ex.getErrMessages());
        modelAndView.setViewName("form");
        return modelAndView;
    }
}
