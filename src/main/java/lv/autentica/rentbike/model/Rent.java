package lv.autentica.rentbike.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "RENTS")
public class Rent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "RENTID")
    private int rentId;

    @Column(name = "NAME")
    private String name;

    @Column(name = "SURNAME")
    private String surname;

    @Column(name = "BIKEID")
    private String bikeId;

    @Column(name = "TIMESTARTED")
    private Date timeStarted;

    @Column(name = "TIMEFINISHED")
    private Date timeFinished;

    public int getRentId() {
        return rentId;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getBikeId() {
        return bikeId;
    }

    public Date getTimeStarted() {
        return timeStarted;
    }

    public Date getTimeFinished() {
        return timeFinished;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setBikeId(String bikeId) {
        this.bikeId = bikeId;
    }

    public void setTimeStarted(Date timeStarted) {
        this.timeStarted = timeStarted;
    }

    public void setTimeFinished(Date timeFinished) {
        this.timeFinished = timeFinished;
    }

}
