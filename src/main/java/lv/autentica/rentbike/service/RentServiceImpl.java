package lv.autentica.rentbike.service;

import lv.autentica.rentbike.dao.RentDao;
import lv.autentica.rentbike.model.Rent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service("rentService")
@Transactional
public class RentServiceImpl implements RentService {

    @Autowired
    private RentDao rentDao;

    @Override
    public void saveRent(Rent rent) {
        rentDao.saveRent(rent);
    }

    @Override
    public List<Rent> findAllRents() {
        return rentDao.findAllRents();
    }

    @Override
    public void deleteRent(int rentId) {
        rentDao.deleteRent(rentId);
    }

    @Override
    public List<Rent> getCurrentBikeRents(String bikeId, Date timeStarted) {
        return rentDao.getCurrentBikeRents(bikeId, timeStarted);
    }

}