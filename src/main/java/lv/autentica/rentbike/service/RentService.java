package lv.autentica.rentbike.service;

import lv.autentica.rentbike.model.Rent;

import java.util.Date;
import java.util.List;

public interface RentService {

    void saveRent(Rent rent);

    List<Rent> findAllRents();

    void deleteRent(int rentId);

    List<Rent> getCurrentBikeRents(String bikeId, Date timeStarted);

}
