package lv.autentica.rentbike.exception;

import java.util.List;

public class ValidationException extends RuntimeException {
    private List<String> errMessages;


    public ValidationException(List<String> errMessages) {
        this.errMessages = errMessages;
    }

    public List<String> getErrMessages() {
        return errMessages;
    }
}
