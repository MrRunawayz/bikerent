package lv.autentica.rentbike.dao;

import lv.autentica.rentbike.model.Rent;

import java.util.Date;
import java.util.List;

public interface RentDao {

    void saveRent(Rent rent);

    List<Rent> findAllRents();

    void deleteRent(int rentId);

    List<Rent> getCurrentBikeRents(String bikeId, Date timeStarted);

}
