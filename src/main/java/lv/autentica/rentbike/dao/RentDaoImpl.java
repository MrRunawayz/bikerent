package lv.autentica.rentbike.dao;

import lv.autentica.rentbike.model.Rent;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository("rentDao")
public class RentDaoImpl extends BaseDao<Integer, Rent> implements RentDao {

    @Autowired
    private SessionFactory sessionFactory;

    private Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public void saveRent(Rent rent) {
        persist(rent);
    }

    @Override
    public List<Rent> findAllRents() {

        Criteria criteria = createEntityCriteria();
        return (List<Rent>) criteria.list();
    }

    public void deleteRent(int rentId) {
        Rent rent = getByKey(rentId);
        if (rent != null)
            getCurrentSession().delete(rent);
    }

    @Override
    public List<Rent> getCurrentBikeRents(String bikeId, Date timeStarted) {
        Criteria criteria = createEntityCriteria();
        return criteria
                .add(Restrictions.eq("bikeId", bikeId))
                .add(Restrictions.ge("timeStarted", timeStarted))
                .list();
    }
}
