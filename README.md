# README #

### Project description ###
* 	Used technologies and frameworks: Java 8, Spring, Hibernate, Maven, MySQL
	
### Project setup, build and run ###
1. 	Clone the repository using Git.
1. 	Create a schema 'bikerent' if not exists using file 'src/main/resources/bikerent.sql'.
1. 	Open the project using IDE and pom.xml file.
1. 	Find file 'src/main/resources/application.properties' and change properties 'jdbc.username' and 'jdbc.password'
1. 	There is Tomcat7 Maven plugin with an embedded Apache Tomcat to run the application. 
1. 	In Intellij IDEA choose Run -> Edit Configurations... -> Add New Configuration -> Choose Maven -> In the tab Parameters -> Command line enter - 'tomcat7:run' -> Click Apply and OK.
1. 	Choose Run -> Run 'Unnamed' (Or other configuration name) to run the application.
1. 	After application has started, follow this URL: http://localhost:8080/

### Using application ###
1. 	Fill the form with data, click OK and see successful results. List of existing rents is shown below the form
1. 	All form fields are required. You have to select the bike from drop down menu which takes values from select options. Additional table for bikes could be added if necessary.
1.  Name and Surname must contain letters only. Started date must not be older than current date-time.
1.  There are validations for checking if bicycle is available for rent, for correct start/finish dates, for empty fields.
1.  You can delete any record by clicking on Delete button